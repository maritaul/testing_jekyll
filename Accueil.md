---
layout: default
title: Accueil
---
# Welcome to the UGA Open Research Data monitor static website _(work in progress)_


## Credit

* Élias Chetouane: collecting data, program automation
* Maxence Larrieu: collecting data, enrichment & visualisation

as members of the [Cellule Data Grenoble Alpes](https://scienceouverte.univ-grenoble-alpes.fr/donnees/accompagner/cellule-data-grenoble-alpes/)
